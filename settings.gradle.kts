rootProject.name = "trixnity"
include("trixnity-core")
include("trixnity-rest-client")
include("trixnity-rest-appservice")
include("examples")
include("examples:multiplatform-ping")
